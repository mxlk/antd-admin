export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', name: 'login', component: './User/Login' },
      { path: '/user/register', name: 'register', component: './User/Register' },
      {
        path: '/user/register-result',
        name: 'register.result',
        component: './User/RegisterResult',
      },
    ],
  },
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      // dashboard
      // { path: '/', redirect: '/dashboard/analysis' },
      {path: '/', redirect: '/home'},
      {
        path: '/home',
        name: 'home',
        icon: 'home',
        component: './Home/Welcome'
      },
      {
        path: '/setting',
        icon: 'setting',
        name: 'setting',
        routes: [
          {
            path: '/setting/sys-notify',
            name: 'sysnotify',
            icon: 'bell',
            component: './Setting/SystemNotify',
          },
          {
            path: '/setting/sys-user',
            name: 'sysuser',
            icon: 'user',
            component: './Setting/SystemUser',
          },
          {
            path: '/setting/sys-role',
            name: 'sysrole',
            icon: 'team',
            component: './Setting/SystemRole',
          },
          {
            path: '/setting/sys-menu',
            name: 'sysmenu',
            icon: 'bars',
            component: './Setting/SystemMenu',
          },
          {
            path: '/setting/sql-monitor',
            name: 'sqlmonitor',
            icon: 'database',
            component: './Setting/SQLMonitor',
          },
          {
            path: '/setting/job-schedule',
            name: 'jobschedule',
            icon: 'clock-circle',
            component: './Setting/JobSchedule',
          },
          {
            path: '/setting/sys-config',
            name: 'sysconfig',
            icon: 'profile',
            component: './Setting/SystemConfig',
          },
          {
            path: '/setting/oss-oss',
            name: 'sysoss',
            icon: 'cloud-upload',
            component: './Setting/SystemOss',
          },
          {
            path: '/setting/sys-log',
            name: 'syslog',
            icon: 'file-text',
            component: './Setting/SystemLog',
          },
        ],
      },
      {
        path: '/merchant',
        name: 'merchant',
        icon: 'contacts',
        routes: [
          {
            path: '/merchant/list',
            name: 'list',
            icon: 'ordered-list',
            component: './Merchant/List',
          },
        ],
      },
      {
        path: '/order',
        name: 'order',
        icon: 'schedule',
        routes: [
          {
            path: '/order/list',
            name: 'list',
            icon: 'ordered-list',
            component: './Order/List',
          },
          {
            path: '/order/settle',
            name: 'settle',
            icon: 'file-done',
            component: './Order/Settle',
          },
          {
            path: '/order/unsettle',
            name: 'unsettle',
            icon: 'file-exclamation',
            component: './Order/Unsettle',
          },
        ],
      },
      {
        name: 'account',
        icon: 'user',
        path: '/account',
        routes: [
          {
            path: '/account/info',
            name: 'info',
            icon: 'idcard',
            component: './Account/Info'
          },
          {
            path: '/account/record',
            name: 'record',
            icon: 'pay-circle',
            component: './Account/Record',
          },
        ],
      },
      {
        name: 'mall',
        icon: 'shop',
        path: '/mall',
        routes: [
          {
            path: '/mall/goods',
            name: 'goods',
            icon: 'shopping',
            component: './Mall/Goods'
          },
          {
            path: '/mall/goodsType',
            name: 'goodsType',
            icon: 'appstore',
            component: './Mall/GoodsType',
          },
          {
            path: '/mall/order',
            name: 'order',
            icon: 'money-collect',
            component: './Mall/Order'
          },
          {
            path: '/mall/goodsUnit',
            name: 'goodsUnit',
            icon: 'tags',
            component: './Mall/GoodsUnit',
          },
        ],
      },
      {
        name: 'overall',
        icon: 'global',
        path: '/overall',
        routes: [
          {
            path: '/overall/notify',
            name: 'notify',
            icon: 'message',
            component: './Overall/Notify'
          },
        ],
      },
      {
        path: '/dashboard',
        name: 'dashboard',
        icon: 'dashboard',
        routes: [
          {
            path: '/dashboard/analysis',
            name: 'analysis',
            component: './Dashboard/Analysis',
          },
          {
            path: '/dashboard/monitor',
            name: 'monitor',
            component: './Dashboard/Monitor',
          },
          // {
          //   path: '/dashboard/workplace',
          //   name: 'workplace',
          //   component: './Dashboard/Workplace',
          // },
          // {
          //   path: '/dashboard/test',
          //   name: 'test',
          //   component: './Dashboard/Test',
          // },
        ],
      },
      // forms
      {
        path: '/form',
        icon: 'form',
        name: 'form',
        routes: [
          {
            path: '/form/basic-form',
            name: 'basicform',
            component: './Forms/BasicForm',
          },
          {
            path: '/form/step-form',
            name: 'stepform',
            component: './Forms/StepForm',
            hideChildrenInMenu: true,
            routes: [
              {
                path: '/form/step-form',
                redirect: '/form/step-form/info',
              },
              {
                path: '/form/step-form/info',
                name: 'info',
                component: './Forms/StepForm/Step1',
              },
              {
                path: '/form/step-form/confirm',
                name: 'confirm',
                component: './Forms/StepForm/Step2',
              },
              {
                path: '/form/step-form/result',
                name: 'result',
                component: './Forms/StepForm/Step3',
              },
            ],
          },
          {
            path: '/form/advanced-form',
            name: 'advancedform',
            authority: ['admin'],
            component: './Forms/AdvancedForm',
          },
        ],
      },
      // list
      {
        path: '/list',
        icon: 'table',
        name: 'list',
        routes: [
          {
            path: '/list/table-list',
            name: 'searchtable',
            component: './List/TableList',
          },
          {
            path: '/list/basic-list',
            name: 'basiclist',
            component: './List/BasicList',
          },
          {
            path: '/list/card-list',
            name: 'cardlist',
            component: './List/CardList',
          },
          {
            path: '/list/search',
            name: 'searchlist',
            component: './List/List',
            routes: [
              {
                path: '/list/search',
                redirect: '/list/search/articles',
              },
              {
                path: '/list/search/articles',
                name: 'articles',
                component: './List/Articles',
              },
              {
                path: '/list/search/projects',
                name: 'projects',
                component: './List/Projects',
              },
              {
                path: '/list/search/applications',
                name: 'applications',
                component: './List/Applications',
              },
            ],
          },
        ],
      },
      {
        path: '/profile',
        name: 'profile',
        icon: 'profile',
        routes: [
          // profile
          {
            path: '/profile/basic',
            name: 'basic',
            component: './Profile/BasicProfile',
          },
          {
            path: '/profile/basic/:id',
            name: 'basic',
            hideInMenu: true,
            component: './Profile/BasicProfile',
          },
          {
            path: '/profile/advanced',
            name: 'advanced',
            authority: ['admin'],
            component: './Profile/AdvancedProfile',
          },
        ],
      },
      {
        name: 'result',
        icon: 'check-circle-o',
        path: '/result',
        routes: [
          // result
          {
            path: '/result/success',
            name: 'success',
            component: './Result/Success',
          },
          { path: '/result/fail', name: 'fail', component: './Result/Error' },
        ],
      },
      {
        name: 'exception',
        icon: 'warning',
        path: '/exception',
        routes: [
          // exception
          {
            path: '/exception/403',
            name: 'not-permission',
            component: './Exception/403',
          },
          {
            path: '/exception/404',
            name: 'not-find',
            component: './Exception/404',
          },
          {
            path: '/exception/500',
            name: 'server-error',
            component: './Exception/500',
          },
          {
            path: '/exception/trigger',
            name: 'trigger',
            hideInMenu: true,
            component: './Exception/TriggerException',
          },
        ],
      },
      {
        component: '404',
      },
    ],
  },
];
