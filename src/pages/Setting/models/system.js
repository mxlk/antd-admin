import { querySysUser, removeSysUser, addSysUser, updateSysUser } from '@/services/api';

export default {
  namespace: 'system',

  state: {
    data: {
      data:{
        dataList: [],
        size: {},
      }
    },
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(querySysUser, payload);
      yield put({
        type: 'save',
        payload: response,
      });
    },
    *add({ payload, callback }, { call, put }) {
      yield call(addSysUser, payload);
      yield put({
        type: 'fetch',
        payload: {},
      });
      if (callback) callback();
    },
    *remove({ payload, callback }, { call, put }) {
      console.log(payload)
      yield call(removeSysUser, payload);
      yield put({
        type: 'fetch',
        payload: {},
      });
      if (callback) callback();
    },
    *update({ payload, callback }, { call, put }) {
      yield call(updateSysUser, payload);
      yield put({
        type: 'fetch',
        payload: {},
      });
      if (callback) callback();
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
};
